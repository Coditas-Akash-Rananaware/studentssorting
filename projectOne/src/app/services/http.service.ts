import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private url = 'https://saral-dev-api.anuvaad.org/getMarksReport';

  constructor(private httpClient: HttpClient) {}

  getData() {
    return this.httpClient.get(this.url);
  }
}
