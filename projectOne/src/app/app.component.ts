import { Component, OnInit } from '@angular/core';
import { IExam } from './app-type/IExam';
import { HttpService } from './services/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  posts: any;
  datesArray: String[] = [];
  newData: Array<Object> = [];
  marks:Array<Number>=[];

  constructor(private httpService: HttpService) {}

  ngOnInit() {
    this.httpService.getData().subscribe((response) => 
    
    
    {
      this.posts = response;
      const { data } = this.posts;
      for (let obj of data) {
        this.datesArray.push(obj.examDate);
      }
      this.datesArray = [...new Set(this.datesArray)];
      this.newData = data;
    });
  }

  getMarks(dateItem: Object) {
    this.marks=[];
      this.newData.forEach((obj: { [x: string]: any }) => {
        if (obj['examDate'] === dateItem) {
          obj['marksInfo'].forEach((mInfo: { [x: string]: any }) => {
            this.marks.push(mInfo['obtainedMarks']);
          });
        }});
   return this.marks;
  }
}
