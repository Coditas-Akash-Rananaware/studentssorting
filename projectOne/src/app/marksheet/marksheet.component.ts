import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-marksheet',
  templateUrl: './marksheet.component.html',
  styleUrls: ['./marksheet.component.scss'],
})
export class MarksheetComponent implements OnInit {
  constructor() {}

  @Input()
  date:String="";
  @Input()
  marks:Array<Number>=[];
  ngOnInit(): void {}
}
 